import boto3
import urllib2
import argparse
#from botocore.exceptions import ClientError
from tabulate import tabulate

'''
Parser for command-line options, arguments and sub-commands
Command to currently supporting:
1. To get all securityGroupName and securityID type python sg.py
2. To check securityGroup present or not type python sg.py -g sg-bf5300c1
3. If present(SecurityGroupName) print all permission related to it type python sg.py -g sg-bf5300c1 -l
4. add ipranges to specific (securityGroupName)python sg.py type -g sg-62f83b05 --add --protocol tcp --port 22 --cidr 182.69.167.242/24
'''


def get_passed_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', default=False, help="To get the SecurityGroups (default: %(default)s)")
    parser.add_argument('-l', nargs='?', default=False, help='To get all the instances (default: %(default)s)')

    parser.add_argument('--add', nargs='?', default=False, help="add IpRanges protocol, port, cidr 'python sg.py "
                                                                "-g sg-62f83b05 --add --protocol tcp --port 22 --cidr "
                                                                "182.69.167.242/24' ")
    parser.add_argument('--protocol', default=False, help="add protocol (default: %(default)s)")
    parser.add_argument('--port', nargs='?', default=False, help='add port (default: %(default)s)')
    parser.add_argument('--cidr', default=False, help="add cidr (default: %(default)s)")
    parser.add_argument('--remove', nargs='?', default=False, help="remove ip range (default: %(default)s)")

    return vars(parser.parse_args())


def get_passed_group_id(value):
    return value['g']


def get_passed_permission(value):
    return value['l']


def get_passed_protocol(value):
    return value['protocol']


def get_port(value):
    return value['port']


def get_cidr(value):
    return value['cidr']


def get_add_ip_range(value):
    return value['add']


def get_remove_ip_range(value):
    return value['remove']


def add_ip_range_count(protocol,  port, cidr, add_ip_range):
    if protocol is not False and port is not False and cidr is not False and add_ip_range is not False:
        return True

    else:
        return False


def remove_ip_range_count(protocol,  port, cidr, remove_ip_range):
    if protocol is not False and port is not False and cidr is not False and remove_ip_range is not False:
        return True
    else:
        return False


def get_count(value):
    args_count = 0
    group_id = get_passed_group_id(value)
    group_permission = get_passed_permission(value)
    protocol = get_passed_protocol(value)
    port = get_port(value)
    cidr = get_cidr(value)
    add_ip_range = get_add_ip_range(value)
    remove_ip_range = get_remove_ip_range(value)

    ip_range_add_count = add_ip_range_count(protocol,  port, cidr, add_ip_range)
    ip_range_remove_count = remove_ip_range_count(protocol,  port, cidr, remove_ip_range)

    if ip_range_remove_count is True:
        args_count = 4
    elif ip_range_add_count is True:
        args_count = 3
    elif group_id and group_permission is not False:
        args_count = 2
    elif group_id is not False:
        args_count = 1
    elif group_permission and group_id is False:
        args_count = 0

    return args_count


# print your IP Address
def current_ip_address():
    response = urllib2.urlopen("http://checkip.amazonaws.com")
    return response.read()


def without_groups(response):
    without_groups_list = []
    for sg in response['SecurityGroups']:
        print sg['GroupId'], sg['GroupName']
        without_groups_list.append([sg['GroupId'], sg['GroupName']])
    return without_groups_list


def with_group(response, value):
    with_group_list = []
    group_id = get_passed_group_id(value)
    for sg in response['SecurityGroups']:
        if group_id is not False:
            if group_id == sg['GroupId']:
                print sg['GroupId'], sg['GroupName']
                with_group_list.append([sg['GroupId'], sg['GroupName']])
    return with_group_list


# return IpPermissions. pass response and groupid to get the ip_permission list
def get_ip_permissions_specific_g(response, value):
    store_ip_permissions_specific_g = []
    for de in response['SecurityGroups']:
        if de['GroupId'] == get_passed_group_id(value):
            for det in de['IpPermissions']:
                store_ip_permissions_specific_g.append(det)
    return store_ip_permissions_specific_g


def chk_ip_ranges(ip_range, ip_ranges_list):
    ip_range_count = len(ip_range['IpRanges'])

    if ip_range_count > 1:
        for x in ip_range['IpRanges']:
            ip_ranges_list.append([ip_range['IpProtocol'], ip_range['FromPort'], ip_range['ToPort'],
                                   x['CidrIp'], ip_range['Ipv6Ranges']])
        return
    else:
        ip_ranges_list.append([ip_range['IpProtocol'], ip_range['FromPort'], ip_range['ToPort'],
                               ip_range['IpRanges'].pop()['CidrIp'], ip_range['Ipv6Ranges']])


# return ip_ranges. pass response and value(groupid) to get the ip_permission list
def get_ip_ranges_specific_g(response, value):
    ip_ranges_list = []
    ip_permission = get_ip_permissions_specific_g(response, value)
    for ip_range in ip_permission:
        if 'FromPort' in ip_range:
            chk_ip_ranges(ip_range, ip_ranges_list)
    return ip_ranges_list


def with__group_permission(response, value):
    with_group(response, value)
    ip_ranges = get_ip_ranges_specific_g(response, value)
    print tabulate(ip_ranges, headers=['Protocol', 'FromPort', 'ToPort', 'IpRange', 'IPv6Range'])


def add_ip_ranges(response, args):

    if args['cidr'][:3] == 'ip/':
        current_ip = current_ip_address().strip() + args['cidr'][2:]
        args['cidr'] = current_ip

    response.authorize_security_group_ingress(
        GroupId=args['g'],
        IpProtocol=args['protocol'],
        FromPort=int(args['port']),
        ToPort=int(args['port']),
        CidrIp=args['cidr']
    )
    return None


def remove_ip_ranges(response, args):

    response.revoke_security_group_ingress(
        GroupId=args['g'],
        IpProtocol=args['protocol'],
        FromPort=int(args['port']),
        ToPort=int(args['port']),
        CidrIp=args['cidr']
    )
    return None


def main():

    try:
        ec2 = boto3.client('ec2')
        response = ec2.describe_security_groups()
        passed_arguments = get_passed_arguments()
        argument_count = get_count(passed_arguments)

        if argument_count is 0:
            without_groups(response)
        elif argument_count is 1:
            with_group(response, passed_arguments)
        elif argument_count is 2:
            with__group_permission(response, passed_arguments)
        elif argument_count is 3:
            response_to_add = ec2
            add_ip_ranges(response_to_add, passed_arguments)
        elif argument_count is 4:
            response_to_add = ec2
            remove_ip_ranges(response_to_add, passed_arguments)
    except OSError as e:
        print e


if __name__ == "__main__":
    main()
