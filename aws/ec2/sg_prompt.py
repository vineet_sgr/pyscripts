from __future__ import unicode_literals
import boto3
from prompt_toolkit.token import Token
from prompt_toolkit.shortcuts import print_tokens
from prompt_toolkit.styles import style_from_dict
from prompt_toolkit import prompt
from tabulate import tabulate

def prompt_call():

    response = {'isContinue': True,
                'value':None,
                'response_text': "Please Enter valid option",
                'response_text_bol': False}

    style = style_from_dict({
        Token.Number: '#884444',
        Token.Text_type: '#884444 italic',
        Token.Title: '#884444'
    })

    tokens = [
        (Token.Title, 'Security Group Options:'),
        (Token, '\n'),
        (Token.Number, '1. '),
        (Token.Text_type, 'List Security Groups'),
        (Token, '\n'),
        (Token.Number, '2. '),
        (Token.Text_type, 'Security Group Details'),
        (Token, '\n'),
        (Token.Number, '3. '),
        (Token.Text_type, 'Add IP Range'),
        (Token, '\n'),
        (Token.Number, '4. '),
        (Token.Text_type, 'Delete IP Range'),
        (Token, '\n'),
        (Token.Number, '5. '),
        (Token.Text_type, 'Exit'),
        (Token, '\n'),
    ]

    our_style = style_from_dict({
        Token.Comment:   '#888888 bold',
        Token.Keyword:   '#ff88ff bold',
    })

    # Print the result.
    print_tokens(tokens, style=style, true_color=True)
    promt_response = prompt("Your choice [1/2/3/4/5]: ", style=our_style)

    if promt_response in ['5']:
        response['isContinue'] = False
    elif promt_response in ['1', '2', '3', '4']:
        response['value'] = promt_response
    else:
        response['response_text_bol'] = True
        print response['response_text']

    return response


def validate_protocol(protocol):
    protocol_supoorted = ["tcp", "udp"]

    if protocol.encode('utf-8') in protocol_supoorted:
        return True
    print "protocol is not valid, either tcp or udp"
    return False


def validate_port(port):

    try:
        if type(int(port)) is not int:
            return False
    except ValueError as error:
        print "Enter Valid Port"
        return False

    if 0 < int(port) < 65536:
        return True
    print "Enter Valid Port"
    return False


def validate_security_group(sg):

    sg_valid = sg[:3]
    if str(sg_valid) == "sg-":
        if sg_valid == "sg-":
            return True

    print "Security Group is not valid, Try again!"
    return False


def validate_cidr(cird):

    print "CIDR is not valid, Try again!"
    return False


def get_promt_args(choice):

    data = {}

    # display security group
    # if choice['value'] == '1':
    #     return data

    # add entry to security group AND remove entry from security group
    if choice['value'] == '3' or choice['value'] == '4':
        data['security_group_id'] = prompt("Security Groupid: ")
        data['protocol'] = prompt("protocol: ")
        data['port'] = prompt("port: ")
        data['cidr'] = prompt("cidr:")
    elif choice['value'] == '2':
        data['security_group_id'] = prompt("Security Groupid:")

    return data


def without_groups(response):
    without_groups_list = []
    for sg in response['SecurityGroups']:
        print sg['GroupId'], sg['GroupName']
        without_groups_list.append([sg['GroupId'], sg['GroupName']])
    return True


def add_ip_ranges(response, args):

    response.authorize_security_group_ingress(
        GroupId=args['security_group_id'],
        IpProtocol=args['protocol'],
        FromPort=int(args['port']),
        ToPort=int(args['port']),
        CidrIp=args['cidr']
    )
    print "IP Range added successfully"
    return True


def remove_ip_ranges(response, args):

    if not validate_protocol(args['protocol']):
        return True

    response.revoke_security_group_ingress(
        GroupId=args['security_group_id'],
        IpProtocol=args['protocol'],
        FromPort=int(args['port']),
        ToPort=int(args['port']),
        CidrIp=args['cidr']
    )
    print "IP Range deleted successfully"
    return True


def security_group_details(response, group_id):
    with_group_list = []
    for sg in response['SecurityGroups']:
        if group_id == sg[u'GroupId']:
            print sg['GroupId'], sg['GroupName']
            with_group_list.append([sg['GroupId'], sg['GroupName']])
    return True


def chk_ip_ranges(ip_range, ip_ranges_list):
    ip_range_count = len(ip_range['IpRanges'])

    if ip_range_count > 1:
        for x in ip_range['IpRanges']:
            ip_ranges_list.append([ip_range['IpProtocol'], ip_range['FromPort'], ip_range['ToPort'],
                                   x['CidrIp'], ip_range['Ipv6Ranges']])
        return
    else:
        ip_ranges_list.append([ip_range['IpProtocol'], ip_range['FromPort'], ip_range['ToPort'],
                               ip_range['IpRanges'].pop()['CidrIp'], ip_range['Ipv6Ranges']])


# return IpPermissions. pass response and groupid to get the ip_permission list
def get_ip_permissions_specific_g(response, group_id):
    store_ip_permissions_specific_g = []
    for de in response['SecurityGroups']:
        if de['GroupId'] == group_id:
            for det in de['IpPermissions']:
                store_ip_permissions_specific_g.append(det)
    return store_ip_permissions_specific_g


def get_ip_ranges_specific_g(response, group_id):
    ip_ranges_list = []
    ip_permission = get_ip_permissions_specific_g(response, group_id)
    for ip_range in ip_permission:
        if 'FromPort' in ip_range:
            chk_ip_ranges(ip_range, ip_ranges_list)
    return ip_ranges_list


def with__group_permission(response, group_id):
    with_group(response, group_id)
    ip_ranges = get_ip_ranges_specific_g(response, group_id)

    if len(ip_ranges) == 0:
        print "--No Data Present--"
        return
    print tabulate(ip_ranges, headers=['Protocol', 'FromPort', 'ToPort', 'IpRange', 'IPv6Range'])


def with_group(response, group_id):
    with_group_list = []
    for sg in response['SecurityGroups']:
        if group_id is not False:
            if group_id == sg['GroupId']:
                print sg['GroupId'], sg['GroupName']
                with_group_list.append([sg['GroupId'], sg['GroupName']])
    return with_group_list


def security_group():
    response_promt = prompt_call()

    if response_promt['isContinue'] is False:
        return False

    if response_promt['response_text_bol'] is True:
        return True

    promt_args = get_promt_args(response_promt)

    validate_list = ['3', '4']

    if response_promt['value'] in validate_list:

        if not validate_security_group(promt_args['security_group_id']):
            return True

        if not validate_protocol(promt_args['protocol']):
            return True

        if not validate_port(promt_args['port']):
            return True

    try:
        ec2 = boto3.client('ec2')
        response = ec2.describe_security_groups()

        if response_promt['value'] == '1':
            without_groups(response)
            return True
        elif response_promt['value'] == '2':
            with__group_permission(response, promt_args['security_group_id'])
            return True
        elif response_promt['value'] == '3':
            ec2_response = ec2
            add_ip_ranges(ec2_response, promt_args)
            return True
        elif response_promt['value'] == '4':
            ec2_response = ec2
            remove_ip_ranges(ec2_response, promt_args)
            return True
    except OSError as e:
        print e


def main():

    is_promt_call = True

    while is_promt_call is True:
        is_promt_call = security_group()


if __name__ == "__main__":
    main()
